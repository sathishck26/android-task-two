package com.karadipath.mydemoapp.ui.product;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.karadipath.mydemoapp.R;
import com.karadipath.mydemoapp.data.pojoresponse.SearchList;
import com.squareup.picasso.Picasso;

public class MyListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private SearchList[] searchLists;
    private boolean isGridView = false;

    public MyListAdapter(Context context, SearchList[] userArrayList, boolean isGridView) {
        this.context = context;
        this.searchLists = userArrayList;
        this.isGridView = isGridView;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView;
        if(isGridView) {
             rootView = LayoutInflater.from(context).inflate(R.layout.product_item_grid,parent,false);
        } else {
             rootView = LayoutInflater.from(context).inflate(R.layout.product_item,parent,false);
        }

        return new RecyclerViewViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        SearchList searchList = searchLists[position];
        RecyclerViewViewHolder viewHolder= (RecyclerViewViewHolder) holder;

        viewHolder.txtView_title.setText(searchList.getProductTitle());
        viewHolder.ageGroup.setText(searchList.getAgeGroup());
        viewHolder.price.setText("₹ "+searchList.getOriginalPrice());
        viewHolder.category.setText(searchList.getCategoryType());
        Picasso.get()
                .load(searchList.getThumbImg())
                .placeholder(R.drawable.ic_image_black_24dp)
                .error(R.drawable.ic_broken_image_black_24dp)
                .into(viewHolder.imgView_icon);
        viewHolder.stock.setVisibility(searchList.getStockStatus().equals("outofstock") ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public int getItemCount() {
        return searchLists.length;
    }

    class RecyclerViewViewHolder extends RecyclerView.ViewHolder {
        ImageView imgView_icon;
        TextView txtView_title;
        TextView ageGroup;
        TextView price;
        TextView category;
        TextView stock;

        public RecyclerViewViewHolder(@NonNull View itemView) {
            super(itemView);
            imgView_icon = itemView.findViewById(R.id.thumbImg);
            txtView_title = itemView.findViewById(R.id.title);
            ageGroup = itemView.findViewById(R.id.ageGroup);
            price = itemView.findViewById(R.id.price);
            category = itemView.findViewById(R.id.category);
            stock = itemView.findViewById(R.id.stockout);
        }
    }
}
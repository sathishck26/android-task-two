package com.karadipath.mydemoapp.ui.product;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.karadipath.mydemoapp.R;
import com.karadipath.mydemoapp.data.pojoresponse.Results;
import com.karadipath.mydemoapp.data.pojoresponse.SearchList;

public class ProductFragment extends Fragment implements ProductListener {
    private RecyclerView recyclerView;
    private MyListAdapter recyclerViewAdapter;
    private SearchList[] list;
    private Context context;
    private Menu menu;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ProductViewModel productViewModel = ViewModelProviders.of(this).get(ProductViewModel.class);
        View root = inflater.inflate(R.layout.fragment_product, container, false);
        setHasOptionsMenu(true);
        recyclerView = root.findViewById(R.id.rView);
        final TextView textView = root.findViewById(R.id.text_gallery);
        productViewModel.mProductListener = this;
        context = this.getActivity();
        productViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

    @Override
    public void onStarted() {
    }

    @Override
    public void onSuccess(Results results) {
        list = results.getSearchList();
        setData(false);
        menu.findItem(R.id.action_List).setVisible(true);
        menu.findItem(R.id.action_grid).setVisible(true);
    }

    @Override
    public void onFailure(String message) {
        Log.d("TAG",message);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menus, @NonNull MenuInflater inflater) {
        menu = menus;
        menu.findItem(R.id.action_List).setVisible(false);
        menu.findItem(R.id.action_grid).setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_List:{
                if(recyclerView != null && recyclerViewAdapter != null) {
                    setData(false);
                }
            }
            break;
            case R.id.action_grid:{
                if(recyclerView != null && recyclerViewAdapter != null) {
                    setData(true);
                }
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setData(boolean isGridView) {
        recyclerView.removeAllViews();
        if (!isGridView) {
            recyclerViewAdapter = new MyListAdapter(context, list, isGridView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
        } else {
            recyclerViewAdapter = new MyListAdapter(context, list, isGridView);
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
            GridLayoutManager gridLayoutManager = new GridLayoutManager(context, 3);
            recyclerView.setLayoutManager(gridLayoutManager);
            recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.HORIZONTAL));
        }
        recyclerView.setAdapter(recyclerViewAdapter);

    }
}

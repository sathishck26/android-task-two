package com.karadipath.mydemoapp.ui.product;

import com.karadipath.mydemoapp.data.pojoresponse.Results;

public interface ProductListener {
    void onStarted();
    void onSuccess(Results results);
    void onFailure(String message);
}

package com.karadipath.mydemoapp.ui.product;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.gson.JsonObject;
import com.karadipath.mydemoapp.data.api.ApiClient;
import com.karadipath.mydemoapp.data.api.RetrofitApi;
import com.karadipath.mydemoapp.data.pojoresponse.Results;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    public ProductListener mProductListener;

    public ProductViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("");
        RetrofitApi retrofitApi =  ApiClient.getClient().create(RetrofitApi.class);
        Call<Results> call =  retrofitApi.getProducts();
        call.enqueue(new Callback<Results>() {
            @Override
            public void onResponse(Call<Results> call, Response<Results> response) {
                Log.d("TAG",""+response.body().toString());
                if(response.code() == 200) {
                    mProductListener.onSuccess(response.body());
                }
            }

            @Override
            public void onFailure(Call<Results> call, Throwable t) {
                Log.e("TAG",""+t);
                mProductListener.onFailure(t.getMessage());
            }
        });
    }

    public LiveData<String> getText() {
        return mText;
    }
}
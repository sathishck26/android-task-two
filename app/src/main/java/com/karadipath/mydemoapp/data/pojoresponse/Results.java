package com.karadipath.mydemoapp.data.pojoresponse;

public class Results {
    private String errcode;

    private String ResultCnt;

    private SearchList[] SearchList;

    private String error;

    public String getErrcode ()
    {
        return errcode;
    }

    public void setErrcode (String errcode)
    {
        this.errcode = errcode;
    }

    public String getResultCnt ()
    {
        return ResultCnt;
    }

    public void setResultCnt (String ResultCnt)
    {
        this.ResultCnt = ResultCnt;
    }

    public SearchList[] getSearchList ()
    {
        return SearchList;
    }

    public void setSearchList (SearchList[] SearchList)
    {
        this.SearchList = SearchList;
    }

    public String getError ()
    {
        return error;
    }

    public void setError (String error)
    {
        this.error = error;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [errcode = "+errcode+", ResultCnt = "+ResultCnt+", SearchList = "+SearchList+", error = "+error+"]";
    }
}

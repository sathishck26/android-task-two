package com.karadipath.mydemoapp.data.pojoresponse;

public class SearchList {
    private String CategoryId;

    private String ThumbImg;

    private String OriginalPrice;

    private String StockStatus;

    private String AgeGroup;

    private String ProductId;

    private String CategoryType;

    private String SKU;

    private String ProductTitle;

    private String FinalPrice;

    public String getCategoryId ()
    {
        return CategoryId;
    }

    public void setCategoryId (String CategoryId)
    {
        this.CategoryId = CategoryId;
    }

    public String getThumbImg ()
    {
        return ThumbImg;
    }

    public void setThumbImg (String ThumbImg)
    {
        this.ThumbImg = ThumbImg;
    }

    public String getOriginalPrice ()
    {
        return OriginalPrice;
    }

    public void setOriginalPrice (String OriginalPrice)
    {
        this.OriginalPrice = OriginalPrice;
    }

    public String getStockStatus ()
    {
        return StockStatus;
    }

    public void setStockStatus (String StockStatus)
    {
        this.StockStatus = StockStatus;
    }

    public String getAgeGroup ()
    {
        return AgeGroup;
    }

    public void setAgeGroup (String AgeGroup)
    {
        this.AgeGroup = AgeGroup;
    }

    public String getProductId ()
    {
        return ProductId;
    }

    public void setProductId (String ProductId)
    {
        this.ProductId = ProductId;
    }

    public String getCategoryType ()
    {
        return CategoryType;
    }

    public void setCategoryType (String CategoryType)
    {
        this.CategoryType = CategoryType;
    }

    public String getSKU ()
    {
        return SKU;
    }

    public void setSKU (String SKU)
    {
        this.SKU = SKU;
    }

    public String getProductTitle ()
    {
        return ProductTitle;
    }

    public void setProductTitle (String ProductTitle)
    {
        this.ProductTitle = ProductTitle;
    }

    public String getFinalPrice ()
    {
        return FinalPrice;
    }

    public void setFinalPrice (String FinalPrice)
    {
        this.FinalPrice = FinalPrice;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [CategoryId = "+CategoryId+", ThumbImg = "+ThumbImg+", OriginalPrice = "+OriginalPrice+", StockStatus = "+StockStatus+", AgeGroup = "+AgeGroup+", ProductId = "+ProductId+", CategoryType = "+CategoryType+", SKU = "+SKU+", ProductTitle = "+ProductTitle+", FinalPrice = "+FinalPrice+"]";
    }
}

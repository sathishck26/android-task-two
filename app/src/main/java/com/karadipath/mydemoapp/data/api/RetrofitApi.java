package com.karadipath.mydemoapp.data.api;

import com.karadipath.mydemoapp.data.pojoresponse.Results;

import retrofit2.Call;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface RetrofitApi {
    @Headers("X-API-KEY: 352e1728bc6344d897c1023f7b9b969c5")
    @POST("search/results/")
    Call<Results> getProducts();
}
